'use strict'

function setSelectedImage(event) {
    const previews = document.getElementsByClassName('image__preview')

    for (let preview of previews) {
        preview.classList.remove('selected')
    }

    event.target.parentNode.classList.add('selected')

    document.getElementById('selected-image').src = event.target.src
    document.getElementById('selected-image-description').textContent = event.target.dataset.description
}
