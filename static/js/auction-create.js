'use strict'

function addImageForm() {
    const totalForms = document.getElementById('id_images-TOTAL_FORMS')

    let totalFormsInt = Number(document.getElementById('id_images-TOTAL_FORMS').value)
    const maxNumFormsInt = Number(document.getElementById('id_images-MAX_NUM_FORMS').value)

    if (totalFormsInt < maxNumFormsInt) {
        const newFormSet = document.getElementById('image-form-template').children.item(0).cloneNode(true)
        newFormSet.innerHTML = newFormSet.innerHTML.replaceAll(/__prefix__/g, totalFormsInt)

        document.getElementById('image-formset-container').appendChild(newFormSet)
        totalForms.value = ++totalFormsInt
    }
}
