'strict'

const csrfToken = Cookies.get("csrftoken")
const stripePubKey = JSON.parse(document.getElementById("stripe-key").textContent);
const successURL = JSON.parse(document.getElementById("success-url").textContent);

const stripe = Stripe(stripePubKey)

fetch(window.location.pathname, {
    method: "POST",
    headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": csrfToken
    }
})
    .then(result => {
        return result.json()
    })
    .then(data => {
        const elements = stripe.elements();

        const card = elements.create("card")
        card.mount("#card-element")

        const form = document.getElementById("payment-form");
        form.addEventListener("submit", event => {
            event.preventDefault();
            payWithCard(stripe, card, data.secret);
        })
    })

function payWithCard(stripe, card, clientSecret) {
    stripe.confirmCardPayment(clientSecret, {
        'payment_method': {
            card: card
        }
    })
        .then(result => {
            if (result.error) {
                // TODO show error to user
                console.log(result.error.message)
            } else {
                window.location.href = successURL
            }
        })
}
