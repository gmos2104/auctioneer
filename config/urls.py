from django.apps import apps
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from apps.auctions import views as auctions_views

urlpatterns = [
    path("", auctions_views.HomeView.as_view(), name="home"),
    path("accounts/", include("apps.accounts.urls")),
    path("accounts/", include("allauth.urls")),
    path("auctions/", include("apps.auctions.urls")),
    path("admin/", admin.site.urls),
]

if settings.DEBUG:
    if apps.is_installed("debug_toolbar"):
        urlpatterns += [path("__debug__/", include("debug_toolbar.urls"))]

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
