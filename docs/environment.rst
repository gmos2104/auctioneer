####################
Environment varibles
####################

The application can be configured via environment variables.

- The following environment variables are required and need to be set for the application to run:

  - ``DB_PASSWORD``
  - ``DB_USER``
  - ``SECRET_KEY``
  - ``STRIPE_LIVE_PUBLIC_KEY``
  - ``STRIPE_LIVE_SECRET_KEY``
  - ``STRIPE_TEST_PUBLIC_KEY``
  - ``STRIPE_TEST_SECRET_KEY``

- The following environment variables are optional but can be set as well:

  - ``ALLOWED_HOSTS`` Default: ['0.0.0.0']
  - ``CACHE_HOST`` Default: localhost
  - ``CACHE_PORT`` Default: 6379
  - ``DB_HOST`` Default: localhost
  - ``DB_NAME`` Default: auctioneer
  - ``DB_PORT`` Default: 5432
  - ``DEBUG`` Default: False
  - ``STRIPE_LIVE_MODE`` Default: False
  - ``USE_DEBUG_TOOLBAR`` Default: False
