######
Docker
######

This project can be run using docker.

==============
Pre-Requisites
==============

- docker

======
Set up
======

#. Create a ``.env`` file and set all the required enviroment variables. You can use ``.env-example-docker`` as a base. See `enviroment variables <docs/environment.rst>`_ for more information.

#. Build the app image and start services: ``docker compose up -d``

#. Apply migrations to the database: ``docker compose run app poetry run python manage.py migrate``

#. (Optional) Create super user: ``docker compose run app poetry run python manage.py createsuperuser``

===================
Running the Project
===================

Start containers:

  ``docker compose start -d``

==============
Basic Commands
==============

- Start services: ``docker compose start``
- Stop services: ``docker compose stop``
- Check service logs: ``docker compose logs -f <service_name>``
- Build app image: ``docker compose build``
