##########
Auctioneer
##########

A playground project to manage auctions made with Django.

Auctioneer is a web app that allows user to create and bid on auctions.

==============
Pre-Requisites
==============

- Install `poetry <https://python-poetry.org/docs/master/#installing-with-the-official-installer>`_.

- Install `tailwindcss cli <https://tailwindcss.com/docs/installation>`_.

- Create a ``.env`` file and set all the required enviroment variables. You can use ``.env-example`` as a base. See `enviroment variables <docs/environment.rst>`_ for more information.

- Have a running PostgreSQL instance.

- Install `Stripe CLI <https://stripe.com/docs/stripe-cli>`_.

Note: You need a Stripe account in order to obtain the live/test keys and use the CLI.

=======
Install
=======

1. Install dependencies:

  ``poetry install``

2. Build css assets:

  ``tailwindcss -i assets/main.css -o static/css/main.css``

3. Initialize Database:

  ``poetry run python manage.py migrate``

(Optional) Create a super user to access admin site:

  ``poetry run python manage.py createsuperuser``

===================
Running the Project
===================

Start the development server run:

  ``poetry run python manage.py runserver``

Start Stripe CLI:

  ``stripe listen --forward-to localhost:8000/auctions/payment/hook``

======
Docker
======

Project can be run using docker. See `Docker <docs/docker.rst>`_ for more details.

========
Features
========

See `TODO <docs/TODO.rst>`_ for planned features/ideas.
