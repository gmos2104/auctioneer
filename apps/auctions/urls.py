from django.urls import path

from .views import (
    AuctionCreateView,
    AuctionDetailView,
    AuctionListView,
    CreateBidView,
    PaymentSuccessView,
    PaymentView,
    payment_webhook,
)

app_name = "auctions"

urlpatterns = [
    path("", AuctionListView.as_view(), name="list"),
    path("<int:pk>", AuctionDetailView.as_view(), name="detail"),
    path("<int:pk>/bid", CreateBidView.as_view(), name="bid"),
    path("create", AuctionCreateView.as_view(), name="create"),
    path("<int:pk>/pay", PaymentView.as_view(), name="pay"),
    path("payment/success", PaymentSuccessView.as_view(), name="payment-successful"),
    path("payment/hook", payment_webhook, name="payment-hook"),
]
