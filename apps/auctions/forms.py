from django.forms import ModelForm, inlineformset_factory

from .models import Auction, AuctionImage, Bid


class AuctionForm(ModelForm):
    class Meta:
        model = Auction
        fields = ["short_description", "description", "initial_price", "end_date"]


AuctionImageFormSet = inlineformset_factory(
    Auction,
    AuctionImage,
    fields=(
        "src",
        "description",
    ),
    min_num=0,
    extra=0,
    max_num=3,
    validate_min=True,
)  # type: ignore


class BidForm(ModelForm):
    class Meta:
        model = Bid
        fields = ["amount"]
