import json

import stripe
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView, DetailView, TemplateView
from django.views.generic.list import ListView

from .forms import AuctionForm, AuctionImageFormSet, BidForm
from .models import Auction, Bid


class HomeView(TemplateView):
    template_name = "home.html"


class AuctionListView(ListView):
    context_object_name = "auctions"
    model = Auction
    paginate_by = 20
    template_name = "auctions/list.html"

    def get_queryset(self):
        queryset = super().get_queryset().filter(status=Auction.Status.NEW)

        if query := self.request.GET.get("q"):
            queryset = queryset.filter(
                Q(short_description__icontains=query) | Q(description__icontains=query)
            )

        return queryset


class AuctionDetailView(DetailView):
    model = Auction
    template_name = "auctions/detail.html"


class AuctionCreateView(LoginRequiredMixin, CreateView):
    form_class = AuctionForm
    template_name = "auctions/create.html"

    def get_context_data(self, **kwargs):
        if kwargs.get("image_formset") is None:
            kwargs["image_formset"] = AuctionImageFormSet(prefix="images")

        return super().get_context_data(**kwargs)

    def form_valid(self, form: AuctionForm) -> HttpResponse:
        auction = form.save(commit=False)
        auction.owner = self.request.user

        image_formset = AuctionImageFormSet(
            self.request.POST, self.request.FILES, instance=auction
        )

        if image_formset.is_valid():
            auction.save()
            image_formset.save()

            self.object = auction

            messages.success(self.request, _("Auction created successfully"))
        else:
            return self.form_invalid(form, image_formset)

        return super().form_valid(form)

    def form_invalid(self, form: AuctionForm, formset=None) -> HttpResponse:
        return self.render_to_response(
            self.get_context_data(form=form, image_formset=formset)
        )


class CreateBidView(LoginRequiredMixin, CreateView):
    form_class = BidForm
    template_name = "auctions/bid.html"

    def get(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        auction = get_object_or_404(Auction, pk=self.kwargs["pk"])

        if self.request.user == auction.owner:
            messages.error(request, _("You own the auction"))
            return HttpResponseRedirect(auction.get_absolute_url())

        if auction.current_bid and self.request.user == auction.current_bid.bidder:
            messages.error(request, _("Already bid on this auction"))
            return HttpResponseRedirect(auction.get_absolute_url())

        return super().get(request, *args, **kwargs)

    def get_success_url(self):
        return reverse("auctions:detail", kwargs={"pk": self.kwargs["pk"]})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        auction = get_object_or_404(Auction, pk=self.kwargs["pk"])

        kwargs["initial"] = {
            "amount": auction.current_price,
        }

        if self.request.method in ("POST", "PUT"):
            kwargs["instance"] = Bid(auction=auction, bidder=self.request.user)

        return kwargs


class PaymentView(LoginRequiredMixin, DetailView):
    model = Auction
    template_name = "auctions/pay.html"

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)

        if settings.STRIPE_LIVE_MODE:
            kwargs["public_key"] = settings.STRIPE_LIVE_PUBLIC_KEY
        else:
            kwargs["public_key"] = settings.STRIPE_TEST_PUBLIC_KEY

        kwargs["success_url"] = reverse("auctions:payment-successful")

        return kwargs

    def post(self, request: HttpRequest, pk: int) -> HttpResponse:
        auction = get_object_or_404(Auction, pk=pk)

        if settings.STRIPE_LIVE_MODE:
            stripe.api_key = settings.STRIPE_LIVE_SECRET_KEY
        else:
            stripe.api_key = settings.STRIPE_TEST_SECRET_KEY

        intent = stripe.PaymentIntent.create(
            amount=int(auction.current_price * 100),
            currency="usd",
            metadata={
                "auction_id": auction.id,
            },
        )

        data = {
            "secret": intent["client_secret"],
        }

        return JsonResponse(data)


class PaymentSuccessView(LoginRequiredMixin, TemplateView):
    template_name = "auctions/payment-successful.html"


@csrf_exempt
def payment_webhook(request: HttpRequest) -> HttpResponse:
    payload = request.body
    event = None

    if settings.STRIPE_LIVE_MODE:
        stripe.api_key = settings.STRIPE_LIVE_SECRET_KEY
    else:
        stripe.api_key = settings.STRIPE_TEST_SECRET_KEY

    try:
        event = stripe.Event.construct_from(json.loads(payload), stripe.api_key)
    except ValueError:
        return HttpResponse(status=400)

    if event.type == "charge.succeeded":
        # On a successfull card charge update the auction status to COMPLETE
        charge = event.data.object
        auction = Auction.objects.get(pk=int(charge["metadata"]["auction_id"]))
        auction.status = Auction.Status.COMPLETE
        auction.save()

    return HttpResponse(status=200)
