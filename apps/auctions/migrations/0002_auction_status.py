# Generated by Django 3.1.3 on 2020-12-07 23:59

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("auctions", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="auction",
            name="status",
            field=models.CharField(
                choices=[
                    ("NEW", "New"),
                    ("CANCELLED", "Cancelled"),
                    ("PAYMENT_PENDING", "Payment Pending"),
                    ("COMPLETE", "Complete"),
                ],
                default="NEW",
                max_length=15,
            ),
        ),
    ]
