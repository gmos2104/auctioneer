from django.contrib import admin

from .models import Auction, AuctionImage, Bid

admin.site.register([Auction, AuctionImage, Bid])
