from decimal import Decimal

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


def media_directory_path(instance, filename):
    return f"{instance.auction.owner_id}/{instance.auction.id}/{filename}"


class Auction(models.Model):
    class Status(models.TextChoices):
        NEW = "NEW", _("New")
        CANCELLED = "CANCELLED", _("Cancelled")
        PAYMENT_PENDING = "PAYMENT_PENDING", _("Payment Pending")
        COMPLETE = "COMPLETE", _("Complete")

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    status = models.CharField(max_length=15, choices=Status.choices, default=Status.NEW)
    short_description = models.CharField(max_length=128)
    description = models.TextField()
    initial_price = models.DecimalField(
        max_digits=8, decimal_places=2, validators=[MinValueValidator(Decimal(1.0))]
    )
    end_date = models.DateTimeField()

    def __str__(self):
        return f"{self.short_description}"

    @property
    def current_price(self):
        bid = self.current_bid
        return bid.amount if bid else self.initial_price

    @property
    def current_bid(self):
        return self.bids.last()

    @property
    def expired(self):
        return timezone.now() >= self.end_date

    def get_absolute_url(self):
        return reverse("auctions:detail", args=[self.id])


class AuctionImage(models.Model):
    auction = models.ForeignKey(
        "Auction", on_delete=models.CASCADE, related_name="images"
    )
    src = models.ImageField(upload_to=media_directory_path)
    description = models.TextField()

    def __str__(self):
        return f"{self.auction.short_description} - {self.description}"


class Bid(models.Model):
    auction = models.ForeignKey(
        "Auction", on_delete=models.CASCADE, related_name="bids"
    )
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    bidder = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-date"]

    def clean(self):
        if self.auction.current_price >= self.amount:
            raise ValidationError(_("Bid amount should be greater than current amount"))
        if self.auction.current_bid and self.auction.current_bid.bidder == self.bidder:
            raise ValidationError(_("Already bid on this auction"))
        if self.auction.expired:
            raise ValidationError(_("Auction has expired"))
        if self.auction.owner == self.bidder:
            raise ValidationError(_("You own the auction"))

    def __str__(self):
        return f"{self.auction.short_description} (${self.amount})"
