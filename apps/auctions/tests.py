from datetime import timedelta

from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .models import Auction, Bid

User = get_user_model()


class TestAuction(TestCase):
    def setUp(self):
        self.user = User.objects.create_user("user", "user@test.com", "password")

    def test_auction_negative_price(self):
        client = Client()
        client.force_login(self.user)
        response = client.post(
            reverse("auctions:create"),
            {
                "short_description": "test",
                "description": "test auction",
                "initial_price": -10,
                "end_date": "08/22/2023",
            },
            follow=True,
        )

        self.assertContains(
            response, "Ensure this value is greater than or equal to 1."
        )


class TestAuctionBids(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user("user1", "user1@test.com", "password")
        self.user_2 = User.objects.create_user("user2", "user2@test.com", "password")
        self.user_3 = User.objects.create_user("user3", "user3@test.com", "password")

        today = timezone.now()

        self.auction_1 = Auction.objects.create(
            owner=self.user_1,
            short_description="user 1 auction",
            description="some description",
            initial_price=10.0,
            end_date=today + timedelta(minutes=15),
        )

        Bid.objects.create(amount=15.0, bidder=self.user_3, auction=self.auction_1)

        self.auction_2 = Auction.objects.create(
            owner=self.user_2,
            short_description="user 2 auction",
            description="some description",
            initial_price=15.0,
            end_date=today - timedelta(minutes=15),
        )

    def test_user_bid(self):
        client = Client()
        client.force_login(self.user_2)
        response = client.post(
            reverse("auctions:bid", args=[self.auction_1.id]),
            {"amount": 15.0},
            follow=True,
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.auction_1.current_price, 15.0)

    def test_error_when_user_is_last_bidder(self):
        client = Client()
        client.force_login(self.user_3)
        response = client.post(
            reverse("auctions:bid", args=[self.auction_1.id]),
            {"amount": 20.0},
            follow=True,
        )

        self.assertContains(response, _("Already bid on this auction"))
        self.assertNotEqual(self.auction_1.current_price, 20.0)

    def test_error_when_bid_amount_is_less_or_equal_than_current_amount(self):
        client = Client()
        client.force_login(self.user_2)
        response = client.post(
            reverse("auctions:bid", args=[self.auction_1.id]),
            {"amount": 10.0},
            follow=True,
        )

        self.assertContains(
            response, _("Bid amount should be greater than current amount")
        )

        response = client.post(
            reverse("auctions:bid", args=[self.auction_1.id]),
            {"amount": 15.0},
            follow=True,
        )

        self.assertContains(
            response, _("Bid amount should be greater than current amount")
        )

    def test_error_when_user_is_auction_owner(self):
        client = Client()
        client.force_login(self.user_1)
        response = client.post(
            reverse("auctions:bid", args=[self.auction_1.id]),
            {"amount": 20.0},
            follow=True,
        )

        self.assertContains(response, _("You own the auction"))
        self.assertNotEqual(self.auction_1.current_price, 20.0)

    def test_error_when_auction_is_expired(self):
        client = Client()
        client.force_login(self.user_1)
        response = client.post(
            reverse("auctions:bid", args=[self.auction_2.id]),
            {"amount": 20.0},
            follow=True,
        )

        self.assertContains(response, _("Auction has expired"))
        self.assertNotEqual(self.auction_2.current_price, 20.0)
