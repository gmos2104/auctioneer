from django.core.management.base import BaseCommand
from django.db.models import Q
from django.utils import timezone

from ...models import Auction


class Command(BaseCommand):
    help = "Update status on finalized auctions"

    def handle(self, *args, **options):
        for auction in Auction.objects.filter(
            Q(status=Auction.Status.NEW) & Q(end_date__lte=timezone.now())
        ).iterator():
            if auction.current_bid:
                auction.status = Auction.Status.PAYMENT_PENDING
            else:
                auction.status = Auction.Status.CANCELLED
            auction.save()
