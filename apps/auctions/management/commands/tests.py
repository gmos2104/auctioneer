from datetime import timedelta

from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone

from ...models import Auction, Bid

User = get_user_model()


class TestAuctionFinalization(TestCase):
    def setUp(self):
        user_1 = User.objects.create_user("user1", "user1@test.com", "password")
        user_2 = User.objects.create_user("user2", "user2@test.com", "password")
        user_3 = User.objects.create_user("user3", "user3@test.com", "password")

        today = timezone.now()

        Bid.objects.create(
            amount=15.0,
            bidder=user_3,
            auction=Auction.objects.create(
                owner=user_1,
                short_description="user 1 auction",
                description="some description",
                initial_price=10.0,
                end_date=today + timedelta(minutes=15),
            ),
        )

        Bid.objects.create(
            amount=20.0,
            bidder=user_3,
            auction=Auction.objects.create(
                owner=user_2,
                short_description="user 2 auction",
                description="some description",
                initial_price=15.0,
                end_date=today - timedelta(minutes=15),
            ),
        )

        Auction.objects.create(
            owner=user_3,
            short_description="user 3 auction",
            description="some description",
            initial_price=12.0,
            end_date=today - timedelta(minutes=15),
        )

        Auction.objects.create(
            owner=user_3,
            short_description="user 3 auction 2",
            description="some description",
            initial_price=12.0,
            end_date=today + timedelta(minutes=15),
        )

    def test_finalize_auctions(self):
        call_command("auctions_finalize")

        auction = Auction.objects.get(short_description="user 1 auction")
        self.assertEqual(auction.status, Auction.Status.NEW)

        auction = Auction.objects.get(short_description="user 2 auction")
        self.assertEqual(auction.status, Auction.Status.PAYMENT_PENDING)

        auction = Auction.objects.get(short_description="user 3 auction")
        self.assertEqual(auction.status, Auction.Status.CANCELLED)

        auction = Auction.objects.get(short_description="user 3 auction 2")
        self.assertEqual(auction.status, Auction.Status.NEW)
