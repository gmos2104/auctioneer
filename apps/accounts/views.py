from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import OuterRef, Q, Subquery
from django.views.generic import TemplateView

from apps.auctions.models import Auction, Bid


class AccountProfileView(LoginRequiredMixin, TemplateView):
    template_name = "account/profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Get auctions created by the user
        auctions = Auction.objects.filter(owner=self.request.user)

        # Get auctions that the user bidded on
        bids = Auction.objects.filter(
            Q(status=Auction.Status.NEW) & Q(bids__bidder=self.request.user)
        ).distinct()

        # Annotate auctions with the current bidder
        current_bidder = Auction.objects.annotate(
            current_bidder=Subquery(
                Bid.objects.filter(auction=OuterRef("pk"))
                .order_by("-date")
                .values("bidder")[:1]
            )
        )

        # Get user pending auctions
        pending = current_bidder.filter(
            Q(status=Auction.Status.PAYMENT_PENDING)
            & Q(current_bidder=self.request.user)
        )

        # Get user completed auctions
        completed = current_bidder.filter(
            Q(status=Auction.Status.COMPLETE) & Q(current_bidder=self.request.user)
        )

        context.update(
            {
                "auctions": auctions,
                "bids": bids,
                "pending": pending,
                "completed": completed,
            }
        )

        return context
