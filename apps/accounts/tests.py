from datetime import timedelta

from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone

from apps.auctions.models import Auction, Bid

User = get_user_model()


class TestUserProfile(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user("user1", "user1@test.com", "password")
        self.user_2 = User.objects.create_user("user2", "user2@test.com", "password")

        today = timezone.now()

        Auction.objects.create(
            owner=self.user_1,
            short_description="user 1 auction",
            description="some description",
            initial_price=10.0,
            end_date=today + timedelta(minutes=15),
        )

        Bid.objects.create(
            amount=15.0,
            bidder=self.user_1,
            auction=Auction.objects.create(
                owner=self.user_2,
                short_description="user 2 auction",
                description="some description",
                initial_price=10.0,
                end_date=today + timedelta(minutes=15),
            ),
        )

        Bid.objects.create(
            amount=15.0,
            bidder=self.user_1,
            auction=Auction.objects.create(
                owner=self.user_2,
                status=Auction.Status.PAYMENT_PENDING,
                short_description="user 2 auction 2",
                description="some description",
                initial_price=10.0,
                end_date=today - timedelta(minutes=15),
            ),
        )

    def test_should_show_user_auctions(self):
        client = Client()
        client.force_login(self.user_1)
        response = client.get(reverse("accounts:profile"))

        self.assertEqual(response.status_code, 200)
        self.assertIn("auctions", response.context)
        self.assertEqual(len(response.context["auctions"]), 1)
        self.assertEqual(
            response.context["auctions"][0].short_description, "user 1 auction"
        )
        self.assertIn("bids", response.context)
        self.assertEqual(len(response.context["bids"]), 1)
        self.assertEqual(
            response.context["bids"][0].short_description, "user 2 auction"
        )
        self.assertIn("pending", response.context)
        self.assertEqual(len(response.context["pending"]), 1)
        self.assertEqual(
            response.context["pending"][0].short_description, "user 2 auction 2"
        )
        self.assertIn("completed", response.context)
        self.assertEqual(len(response.context["completed"]), 0)
