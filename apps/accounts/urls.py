from django.urls import path

from .views import AccountProfileView

app_name = "accounts"

urlpatterns = [
    path("profile", AccountProfileView.as_view(), name="profile"),
]
