FROM python:3.12

ARG PUID=1000
ARG PGID=1000

ENV PUID ${PUID}
ENV PGID ${PGID}
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE "config.settings"

RUN apt-get update\
    && apt-get -y install gettext

RUN groupadd -g ${PGID} django \
    && useradd -u ${PUID} -g django -m django -s /bin/bash

USER django

RUN curl -sSL https://install.python-poetry.org | python -

ENV IN_DOCKER True

ENV PATH "${PATH}:/home/django/.local/bin"

WORKDIR /home/django/code

COPY pyproject.toml poetry.lock ./

RUN poetry install

CMD poetry run python manage.py runserver 0.0.0.0:8000

EXPOSE 8000
